import java.util.Scanner;
/*
 * Austin Rafter
 * 013433901
 * 10/02/2020
 *
 * print appointments on user entered day
 */

public class P9_3 {

    public static void main(String[] args) {

        Appointment appointmentDentist = new Monthly("Dentist", "2020-11-09");
        Appointment appointmentDentistTwo = new Daily("Doctor", "2020-11-09");
        Appointment appointmentDentistThree = new Onetime("Eat a sandwich", "2020-11-09");
        Appointment[] appointmentsChecks =  new Appointment[3];

        appointmentsChecks[0] = appointmentDentist;
        appointmentsChecks[1] = appointmentDentistTwo;
        appointmentsChecks[2] = appointmentDentistThree;
        System.out.print("Enter date(yyyy-mm-dd):");
        Scanner scanObject = new Scanner(System.in);
        String strDateEntered = scanObject.nextLine();

        String[] splitToInts = strDateEntered.split("-");

        int nYearCompare = Integer.parseInt(splitToInts[0]);
        int nMonthCompare = Integer.parseInt(splitToInts[1]);
        int nDayCompare = Integer.parseInt(splitToInts[2]);

        for(int i =0; i< appointmentsChecks.length; i++ ) {
            if(appointmentsChecks[i].occursOn(nYearCompare, nMonthCompare, nDayCompare)){
                System.out.println("You have these appointments that day " + appointmentsChecks[i].getAppointmentDescription());
            }
        }



    }
}
